package be.kdg.prog12.io;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class IoDemo {

	private static final String FILE= "film.txt";

	public static void main(String[] args) {
		//useInputStream();
	//	useInputStreamReader();
	//	useOutputStreamWriter();
	//	useWriter();
	//	useBufferedWriter();
		useZipWriter();
	}

	private static void useZipWriter() {
		try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(FILE));
		     ZipOutputStream out = new ZipOutputStream(new FileOutputStream("ZipOutputstream_" + FILE + ".zip"));
		) {
			out.putNextEntry(new ZipEntry(FILE));
			int c = in.read();
			while (c != -1) {
				out.write(c );
				c = in.read() ;
			}
			out.closeEntry();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void useBufferedWriter() {
		try (BufferedReader in = new BufferedReader(new FileReader(FILE));
		     BufferedWriter out = new BufferedWriter(new FileWriter("BufferedWriter_" + FILE));
		) {
			String c = in.readLine();
			while (c != null) {
				out.write(c + "\n");
				c = in.readLine() ;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void useWriter() {
		try (Reader in = new FileReader(FILE);
		     Writer out = new FileWriter("Writer_" + FILE);
		) {
			int c = in.read();
			while (c != -1) {
				out.write(c);
				c = in.read();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void useOutputStreamWriter() {
		try (Reader in = new InputStreamReader(new FileInputStream(FILE));
		     Writer out = new OutputStreamWriter(new FileOutputStream("OutputstreamWriter_" + FILE));
		) {
			int c = in.read();
			while (c != -1) {
				out.write(c);
				c = in.read();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void useInputStreamReader() {
		try (Reader in = new InputStreamReader(new FileInputStream(FILE));
		     OutputStream out = new FileOutputStream("InputstreamReader_" + FILE);
		) {
			int c = in.read();
			while (c != -1) {
				out.write(c);
				c = in.read();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void useInputStream() {
		try(InputStream in=new FileInputStream(FILE);
		    OutputStream out= new FileOutputStream("Ouputstream_"+FILE);
		    ) {
			int c=in.read();
			while(c != -1){
				out.write(c);
				c=in.read();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


}
